public class Main {
    public static void main(String[] args) {
        int iteration = 5;

        // first loop
        for(int i = 0; i < iteration; i++) {
            for(int j = 0; j <= i; j++) {
                System.out.print("*");
            }
            System.out.print("\n");
        }
        System.out.print("\n");

        // second loop
        for(int i = iteration; i > 0; i--) {
            for(int j = i; j > 0; j--) {
                System.out.print("*");
            }
            System.out.print("\n");
        }
        System.out.print("\n");

        // third loop
        for(int i = 0; i < iteration; i++) {
            for(int j = i; j < iteration - 1; j++) {
                System.out.print(" ");
            }
            for(int k = 0; k <= i; k++) {
                System.out.print("*");
            }
            System.out.print("\n");
        }
        System.out.print("\n");

        // forth loop
        for(int i = 0; i < iteration; i++) {
            for(int j = 0; j < i; j++) {
                System.out.print(" ");
            }
            for(int k = i; k < iteration; k++) {
                System.out.print("*");
            }
            System.out.print("\n");
        }
    }
}
