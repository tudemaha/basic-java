package id.my.tudemaha;

import java.io.*;
import java.nio.Buffer;
import java.security.spec.ECField;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Main {
    public static void main(String[] args) throws IOException {
        Scanner userChoice = new Scanner(System.in);
        String choice;
        boolean isRepeat = true;

        while(isRepeat) {
            clearConsole();
            System.out.println("===== PROGRAM PERPUSTAKAAN =====");
            System.out.println("================================\n");

            System.out.println("Menu:");
            System.out.println("1. Lihat Data Buku");
            System.out.println("2. Cari Data Buku");
            System.out.println("3. Tambah Data Buku");
            System.out.println("4. Ubah Data Buku");
            System.out.println("5. Hapus Data Buku");
            System.out.println("6. Keluar");

            System.out.print("Masukkan pilihan: ");
            choice = userChoice.next();

            switch(choice) {
                case "1":
                    System.out.println("\n================");
                    System.out.println("DAFTAR DATA BUKU");
                    System.out.println("================");

                    readData();
                    break;
                case "2":
                    System.out.println("\n==============");
                    System.out.println("CARI DATA BUKU");
                    System.out.println("==============");

                    searchData();
                    break;
                case "3":
                    System.out.println("\n===============");
                    System.out.println("TAMBAH DATA BUKU");
                    System.out.println("================");


                    break;
                case "4":
                    break;
                case "5":
                    break;
                default:
                    System.err.println("Masukan tidak valid. Pilih 1-6!\n");
                    break;
            }

            isRepeat = getConfirmation("Ingin mengulang");
        }
    }

    private static void readData() throws IOException {
        FileReader fileInput;
        BufferedReader bufferInput;
        try {
            fileInput = new FileReader("database.txt");
            bufferInput = new BufferedReader(fileInput);
        } catch(Exception e) {
            System.err.println("Tidak ada database file, hubungi admin.");
            return;
        }

        String data = bufferInput.readLine();
        int count = 0;
        System.out.println("0. Nama Pengarang - Tahun Terbit - Penerbit - Judul Buku");
        while(data != null) {
            count++;
            printData(count, data);
            data = bufferInput.readLine();
        }
    }

    public static void searchData() throws IOException {
        // check file availability
        File bookFile = new File("database.txt");
        if(!bookFile.exists()) {
            throw new IOException("File tidak tersedia, hubungi admin!");
        }

        // input keywords from user
        System.out.print("Masukkan kata kunci pencarian: ");
        Scanner userInput = new Scanner(System.in);
        String inputString = userInput.nextLine();
        String[] keywords = inputString.split("\\s+");

        searchInDatabase(keywords);
    }

    private static void searchInDatabase(String[] keywords) throws IOException {
        // prepare database
        FileReader fileInput = new FileReader("database.txt");
        BufferedReader inputBuffer = new BufferedReader(fileInput);

        // check if data per line contains the keyword
        String data = inputBuffer.readLine();
        boolean isAvailable;
        int count = 0;

        System.out.println("0. Nama Pengarang - Tahun Terbit - Penerbit - Judul Buku");
        while(data != null) {

            isAvailable = true;

            // if user inserts more than 2 keyword, both must available
            for(String keyword : keywords) {
                keyword = keyword.toLowerCase();
                isAvailable = isAvailable && data.toLowerCase().contains(keyword);
            }

            if(isAvailable) {
                count++;
                printData(count, data);
            }

            data = inputBuffer.readLine();
        }
    }

    private static void printData(int count, String data) {
        // print data based on csv file
        StringTokenizer stringParser = new StringTokenizer(data, ";");
        System.out.printf("%d. ", count);
        stringParser.nextToken();
        System.out.printf("%s - ", stringParser.nextToken());
        System.out.printf("%s - ", stringParser.nextToken());
        System.out.printf("%s - ", stringParser.nextToken());
        System.out.println(stringParser.nextToken());
    }

    private static void clearConsole() {
        try {
            if(System.getProperty("os.name").contains("Windows")) {
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            } else {
                System.out.println("\033[H\033[2J");
            }
        } catch(Exception e) {
            System.err.print("Gagal membersihkan console.");
        }
    }

    private static boolean getConfirmation(String message) {
        System.out.print("\n" + message + "? (y/n): ");
        Scanner userChoice = new Scanner(System.in);
        String choice = userChoice.next();

        while(!choice.equalsIgnoreCase("y") && !choice.equalsIgnoreCase("n")) {
            System.out.print(message + "? (y/n): ");
            choice = userChoice.next();
        }

        return choice.equalsIgnoreCase("y");
    }
}
