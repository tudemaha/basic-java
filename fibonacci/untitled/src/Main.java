import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int fibo1 = 0;
        int fibo2 = 1;
        int temp;

        System.out.print("Banyak deret Fibonacci: ");
        int iterasi = input.nextInt();

        for(int i = 0; i < iterasi; i++) {
            System.out.println(fibo1);

            temp = fibo1 + fibo2;
            fibo1 = fibo2;
            fibo2 = temp;
        }
    }
}
