package my.id.tudemaha;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        mainMenu();
        System.out.print("Masukkan pilihan: ");
        char choice = input.next().charAt(0);

        if(choice != '1' && choice != '2') {
            System.err.println("Pilihan tidak valid");
            return;
        }

        int[][] matrix1 = insertMatrix();
        int[][] matrix2 = insertMatrix();

        switch(choice) {
            case '1':
                try {
                    int[][] result = addition(matrix1, matrix2);
                    System.out.println("\nHasil penjumlahan:");
                    printMatrix(result);
                } catch(Exception e) {
                    System.err.println(e.getMessage());
                }
                break;
            case '2':
                try {
                    int[][] result = multiplication(matrix1, matrix2);
                    System.out.println("\nHasil perkalian:");
                    printMatrix(result);
                } catch(Exception e) {
                    System.err.println(e.getMessage());
                }
                break;
            default:
                System.out.println("\nPilihan tidak valid");
                break;
        }
    }

    private static int[][] insertMatrix() {
        Scanner input = new Scanner(System.in);

        System.out.print("\nBaris: ");
        int row = input.nextInt();
        System.out.print("Kolom: ");
        int column = input.nextInt();

        int[][] matrix = new int[row][column];
        for(int i = 0; i < row; i++) {
            for(int j = 0; j < column; j++) {
                System.out.printf("Elemen [%d][%d]: ", i + 1, j + 1);
                matrix[i][j] = input.nextInt();
            }
        }

        return matrix;
    }

    private static void mainMenu() {
        System.out.println("===== PROGRAM MATRIKS SEDERHANA =====");
        System.out.println("=====================================");
        System.out.print("Menu:\n1. Penjumlahan\n2. Perkalian\n");
    }

    // matrix addition
    private static int[][] addition(int[][] matrix1, int[][] matrix2) throws Exception{
        int matrix1_row = matrix1.length;
        int matrix1_col = matrix1[0].length;
        int matrix2_row = matrix2.length;
        int matrix2_col = matrix2[0].length;
        int[][] result = new int[matrix1_row][matrix1_col];

        if(matrix1_row != matrix2_row || matrix1_col != matrix2_col) {
            throw new Exception("Different row or column number");
        } else {
            for(int i = 0; i < matrix1_row; i++) {
                for(int j = 0; j < matrix1_col; j++) {
                    result[i][j] = matrix1[i][j] + matrix2[i][j];
                }
            }
        }

        return result;
    }

    // matrix multiplication
    private static int[][] multiplication(int[][] matrix1, int[][] matrix2) throws Exception {
        int matrix1_row = matrix1.length;
        int matrix1_col = matrix1[0].length;
        int matrix2_row = matrix2.length;
        int matrix2_col = matrix2[0].length;
        int[][] result = new int[matrix1_row][matrix2_col];

        if(matrix1_col != matrix2_row) {
            throw new Exception("Row and column number not match");
        } else {
            for(int i = 0; i < matrix1_row; i++) {
                for(int j = 0; j < matrix2_col; j++) {
                    for(int k = 0; k < matrix2_row; k++) {
                        result[i][j] += matrix1[i][k] * matrix2[k][j];
                    }
                }
            }
        }

        return result;
    }

    // print matrix
    private static void printMatrix(int[][] matrix){
        for(int i = 0; i < matrix.length; i++) {
            System.out.print("[");
            for(int j = 0; j < matrix[0].length; j++) {
                System.out.print(matrix[i][j]);
                if(j < matrix[0].length - 1) System.out.print(", ");
            }
            System.out.println("]");
        }
    }
}
